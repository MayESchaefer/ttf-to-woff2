from fontTools.ttLib import TTFont
from os import walk
import os
from pathlib import Path
import sys

filenames = next(walk('.'), (None, None, []))[2]
for filename in filenames:
	print(filename)
	f = TTFont(filename)
	f.flavor='woff2'
	f.save(os.path.splitext(filename)[0]+'.woff2')